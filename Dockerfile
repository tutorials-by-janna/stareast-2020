FROM golang:1.13

# Set paths
WORKDIR /go/src/app
COPY . .

# Download dependencies and install app
RUN go get -d -v ./...
RUN go install -v ./...

# Install beego and the bee dev tool
RUN go get github.com/astaxie/beego && go get github.com/beego/bee

# Expose the application on port 8080
EXPOSE 8080

# Set the entry point of the container to the bee command that runs the
# application and watches for changes
CMD ["bee", "run"]