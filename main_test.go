package main

import (
	"net/http"
	"testing"
)

func TestGetName(t *testing.T) {

	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("null name", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/", nil)

		got := GetName(request)
		want := "Anonymous"

		assertCorrectMessage(t, got, want)
	})

	t.Run("personalized message", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/?name=Janna", nil)

		got := GetName(request)
		want := "Janna"

		assertCorrectMessage(t, got, want)
	})

}
