package main

import (
	"fmt"
	"html/template"
	"net/http"
	"time"
)

//Create a struct that holds information to be displayed in our HTML file
type Welcome struct {
	Name string
	Time string
}

//Go application entrypoint
func main() {

	//Instantiate a Welcome struct object and pass in some random information.
	welcome := Welcome{"Anonymous", time.Now().Format(time.Stamp)}

	//Tell Go exactly where we can find our html file. Parse the html file. We wrap it in a call to template.Must()
	//which handles any errors and halts if there are fatal errors
	templates := template.Must(template.ParseFiles("templates/welcome-template.html"))

	//Tell go to create a handle that looks in the static directory, go then uses the "/static/" as a url that our
	//html can refer to when looking for our css and other files.

	http.Handle("/static/", //final url can be anything
		http.StripPrefix("/static/",
			http.FileServer(http.Dir("static"))))

	//This method takes in the URL path "/" and a function that takes in a response writer, and a http request.
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		welcome.Name = GetName(r)

		//If errors show an internal server error message
		//I also pass the welcome struct to the welcome-template.html file.
		if err := templates.ExecuteTemplate(w, "welcome-template.html", welcome); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	//Start the web server, set the port to listen to 8080. Without a path it assumes localhost
	//Print any errors from starting the webserver using fmt
	fmt.Println("Listening on Port 8080")
	fmt.Println(http.ListenAndServe(":8080", nil))
}

func GetName(r *http.Request) string {
	//Takes the name from the URL query e.g ?name=Martin, will set welcome.Name = Martin.
	if name := r.FormValue("name"); name != "" {
		return name
	}

	return "Anonymous"
}
